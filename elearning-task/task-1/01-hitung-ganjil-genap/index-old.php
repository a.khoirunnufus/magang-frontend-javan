<?php

$result = array();

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $a = intval($_POST['a']);
    $b = intval($_POST['b']);

    for ($i = $a; $i <= $b; $i++) {
        if (fmod($i, 2) == 0) {
            array_push($result, "<em>Angka {$i} adalah genap</em>");
        } else {
            array_push($result, "<em>Angka {$i} adalah ganjil</em>");
        }
    }
}

?>

<html>
<body>

<h1>Input Form</h1>
<form method="post" action="<?php echo $_SERVER["PHP_SELF"];?>">
    <input type="number" name="a" placeholder="first number" required /><br>
    <input type="number" name="b" placeholder="second number" required /><br>
    <input type="submit" value="Submit" />
</form>

<h1>Result</h1>
<div id="result">
    <?php foreach($result as $value): ?>
        <p><?= $value; ?></p>
    <?php endforeach; ?>
</div>

</body>
</html>
