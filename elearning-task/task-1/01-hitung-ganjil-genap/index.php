<?php


function hitung_ganjil_genap($a, $b)
{
    $result = array();

    for ($i = $a; $i <= $b; $i++) {
        if (fmod($i, 2) == 0) {
            array_push($result, "<em>Angka {$i} adalah genap</em>");
        } else {
            array_push($result, "<em>Angka {$i} adalah ganjil</em>");
        }
    }

    return $result;
}

echo "A=1, B=4 : <br>";
foreach(hitung_ganjil_genap(1, 4) as $res)
{
    echo $res . "<br>";
}

echo "<br>";

echo "A=10, B=11 : <br>";
foreach(hitung_ganjil_genap(10, 11) as $res)
{
    echo $res . "<br>";
}

echo "<br>";

echo "A=6, B=8 : <br>";
foreach(hitung_ganjil_genap(6, 8) as $res)
{
    echo $res . "<br>";
}