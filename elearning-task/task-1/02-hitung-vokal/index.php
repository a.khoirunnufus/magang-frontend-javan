<?php

function hitung_vokal($param_kata)
{
    $kata_arr = str_split(strtolower($param_kata));
    $vokal_arr = array('a', 'i', 'u', 'e', 'o');
    $res_vokal_arr = array();
    $res_jumlah_vokal = 0;

    foreach($kata_arr as $huruf)
    {
        if(in_array($huruf, $vokal_arr) && !in_array($huruf, $res_vokal_arr))
        {
            array_push($res_vokal_arr, $huruf);
            $res_jumlah_vokal ++;
        }
    }

    return array(
        'jumlah_vokal' => $res_jumlah_vokal,
        'huruf_vokal' => $res_vokal_arr
    );
}

$wisnu = hitung_vokal('wisnu');
echo "Input: 'wisnu'<br>";
echo "Jumlah Vokal: " . $wisnu['jumlah_vokal'] . "<br>";
echo "Huruf vokal: " . implode(', ', $wisnu['huruf_vokal']) . "<br>";

echo "<br><br>";

$bayu = hitung_vokal('bayu');
echo "Input: 'bayu'<br>";
echo "Jumlah Vokal: " . $bayu['jumlah_vokal'] . "<br>";
echo "Huruf vokal: " . implode(', ', $bayu['huruf_vokal']) . "<br>";

echo "<br><br>";

$manupraba = hitung_vokal('manupraba');
echo "Input: 'manupraba'<br>";
echo "Jumlah Vokal: " . $manupraba['jumlah_vokal'] . "<br>";
echo "Huruf vokal: " . implode(', ', $manupraba['huruf_vokal']) . "<br>";