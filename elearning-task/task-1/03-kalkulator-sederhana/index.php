<?php

function kalkulator($string_operasi)
{
    $string_arr = explode(' ', $string_operasi);
    $operand_a = floatval($string_arr[0]);
    $operand_b = floatval($string_arr[2]);
    $operator = $string_arr[1];
    $result = null;

    try
    {
        switch ($operator)
        {
            case "+":
                $result = $operand_a + $operand_b;
                break;
            case "-":
                $result = $operand_a - $operand_b;
                break;
            case "*":
                $result = $operand_a * $operand_b;
                break;
            case "/":
                if($operand_b == 0) throw new Exception();
                $result = $operand_a / $operand_b;
                break;
            default:
                throw new Exception();
        }
    }
    catch (Exception $e)
    {
        $result = 'tidak bisa dilakukan';
    }

    return $result;
}

echo "Input: '2 + 2'<br>";
echo "Output: " . kalkulator('2 + 2') . "<br><br>";

echo "Input: '2 * 2'<br>";
echo "Output: " . kalkulator('2 * 2') . "<br><br>";

echo "Input: '2 / 2'<br>";
echo "Output: " . kalkulator('2 / 2') . "<br><br>";

echo "Input: '2 / 0'<br>";
echo "Output: " . kalkulator('2 / 0') . "<br><br>";