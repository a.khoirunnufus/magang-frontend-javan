# query CEO
select * from employee
where atasan_id is null;



# query Staff Biasa
select *
from employee
where id not in (
  select distinct atasan_id
  from employee
  where atasan_id is not null
);



# query Direktur
select * from employee
where atasan_id = (
  select id from employee
  where atasan_id is null
);



# query Manager
with direktur_cte as (
  select * from employee
  where atasan_id = (
    select id from employee
    where atasan_id is null
  )
)
select * from employee
where atasan_id in (
  select id from direktur_cte
);



# query jumlah bawahan
with recursive atasan_cte(id, nama, atasan_id)
as (
  select id, nama, atasan_id
  from employee
  where nama = 'Bu Sinta'
  union all
  select e.id, e.nama, e.atasan_id
  from employee e
    join atasan_cte a on e.atasan_id = a.id
)
select count(*) as jumlah_bawahan from atasan_cte
where nama != 'Bu Sinta';