start transaction;

# get Rizki Saputra id
# id = 16
select id from karyawan
where nama = 'Rizki Saputra';

# update karyawan supervisor
update karyawan
set supervisor_id = 16
where
  nama = 'Farhan Reza'
  or nama = 'Riyando Adi';

# get departemen id
# Pengembangan Bisnis, id = 2
# Teknisi, id = 3
# Analis, id = 4
select * from departemen;

# id Farhan Reza = 17
# id Riyando Adi = 18
select * from karyawan;

# update karyawan supervisor
update karyawan
set supervisor_id = 17
where departemen = 2 or departemen = 3;

update karyawan
set supervisor_id = 18
where departemen = 4;

commit;