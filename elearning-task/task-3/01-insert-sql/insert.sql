# insert to departemen table
insert into departemen (nama) values ('Manajemen'), ('Pengembangan Bisnis'), ('Teknisi'), ('Analis');

# insert to karyawan table
insert into karyawan (nama, `jenis kelamin`, status, `tanggal lahir`, `tanggal masuk`, departemen)
values ('Rizki Saputra', 'L', 'Menikah', str_to_date('10/11/1980', '%m/%d/%Y'), str_to_date('1/1/2011', '%m/%d/%Y'), 1),
       ('Farhan Reza', 'L', 'Belum', str_to_date('11/1/1989', '%m/%d/%Y'), str_to_date('1/1/2011', '%m/%d/%Y'), 1),
       ('Riyando Adi', 'L', 'Menikah', str_to_date('1/25/1977', '%m/%d/%Y'), str_to_date('1/1/2011', '%m/%d/%Y'), 1),
       ('Diego Manuel', 'L', 'Menikah', str_to_date('2/22/1983', '%m/%d/%Y'), str_to_date('9/4/2012', '%m/%d/%Y'), 2),
       ('Setya Laksana', 'L', 'Menikah', str_to_date('1/12/1981', '%m/%d/%Y'), str_to_date('3/19/2011', '%m/%d/%Y'), 2),
       ('Miguel Hernandez', 'L', 'Menikah', str_to_date('10/16/1994', '%m/%d/%Y'), str_to_date('6/15/2014', '%m/%d/%Y'), 2),
       ('Putri Persada', 'P', 'Menikah', str_to_date('1/30/1988', '%m/%d/%Y'), str_to_date('4/14/2013', '%m/%d/%Y'), 2),
       ('Alma Safira', 'P', 'Menikah', str_to_date('5/18/1991', '%m/%d/%Y'), str_to_date('9/28/2013', '%m/%d/%Y'), 3),
       ('Haqi Hafiz', 'L', 'Belum', str_to_date('9/19/1995', '%m/%d/%Y'), str_to_date('3/9/2015', '%m/%d/%Y'), 3),
       ('Abi Isyawara', 'L', 'Belum', str_to_date('6/3/1991', '%m/%d/%Y'), str_to_date('1/22/2012', '%m/%d/%Y'), 3),
       ('Maman Kresna', 'L', 'Belum', str_to_date('8/21/1993', '%m/%d/%Y'), str_to_date('9/15/2012', '%m/%d/%Y'), 3),
       ('Nadia Aulia', 'P', 'Belum', str_to_date('10/7/1989', '%m/%d/%Y'), str_to_date('5/7/2012', '%m/%d/%Y'), 4),
       ('Mutiara Rezki', 'P', 'Menikah', str_to_date('3/23/1988', '%m/%d/%Y'), str_to_date('5/21/2013', '%m/%d/%Y'), 4),
       ('Dani Setiawan', 'L', 'Belum', str_to_date('2/11/1986', '%m/%d/%Y'), str_to_date('11/30/2014', '%m/%d/%Y'), 4),
       ('Budi Putra', 'L', 'Belum', str_to_date('10/23/1995', '%m/%d/%Y'), str_to_date('12/3/2015', '%m/%d/%Y'), 4);
