i paling awal
select nama from kerajaan
where parent_id is null;

# nama anak
select ka.nama, group_concat(kb.nama) as anak
from kerajaan ka
  inner join kerajaan kb on kb.parent_id = ka.id
group by ka.nama;

# nama kakek
with kn_cte
as
(
  select
    ka.nama,
    (
      select kc.nama
      from kerajaan kb
        inner join kerajaan kc on kc.id = kb.parent_id
      where kb.id = kd.id
    ) as kakek_nenek
  from kerajaan ka
    inner join kerajaan kd on kd.id = ka.parent_id
)
select * from kn_cte
where kakek_nenek is not null;

# nama cucuk
with cucu_cte
as
(
 select kc.nama,
  (
    select group_concat(kb.nama)
    from kerajaan ka
     inner join kerajaan kb on kb.parent_id = ka.id
    where ka.id = kd.id
    group by ka.nama
  ) as cucu
 from kerajaan kc
  inner join kerajaan kd on kd.parent_id = kc.id
)
select * from cucu_cte
where cucu is not null;

# jumlah anak
select ka.nama, count(kb.nama) as jumlah_anak
from kerajaan ka
  inner join kerajaan kb on kb.parent_id = ka.id
group by ka.nama;

# jumlah keturunan
with recursive keturunan_ext(id, nama, count) as (
  select id, nama, 1
  from kerajaan
  union all
  select ka.id, ke.nama, 1 as count
  from keturunan_ext ke
    join kerajaan ka on ke.id = ka.parent_id
)
select nama, count(*) as jumlah_anak from keturunan_ext
group by nama
order by nama;

# jumlah keturunan berdasarkan kelamin
with recursive keturunan_ext(id, nama, kelamin, count) as (
  select id, nama, kelamin, 1
  from kerajaan
  union all
  select ka.id, ke.nama, ka.kelamin, 1 as count
  from keturunan_ext ke
    join kerajaan ka on ke.id = ka.parent_id
)
select nama, kelamin, count(*) as jumlah_anak from keturunan_ext
group by nama, kelamin
order by nama, kelamin;
