<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class OddEvenController extends Controller
{
    public function index()
    {
        return view('odd-even');
    }

    public function generate(Request $request)
    {
        $result = array();
        $a = intval($request->input('a'));
        $b = intval($request->input('b'));

        for ($i = $a; $i <= $b; $i++) {
            if (fmod($i, 2) == 0) {
                array_push($result, [ 'number' => $i, 'odd_even' => 'even' ]);
            } else {
                array_push($result, [ 'number' => $i, 'odd_even' => 'odd' ]);
            }
        }

        return json_encode($result);
    }
}
