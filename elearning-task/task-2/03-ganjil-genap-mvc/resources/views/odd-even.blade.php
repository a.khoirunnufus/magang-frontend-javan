<html>
<head>
    <title>Odd Even MVC</title>

    <style>
        body {
            font-size: 16px;
            font-family: sans-serif;
        }
        input {
            border: 1px solid lightgray;
            padding: .5rem 1rem;
            font-size: 1rem;
            border-radius: 10px;
            outline: none;
            display: block;
            height: 50px;
        }
        button {
            background-color: dodgerblue;
            padding: 1rem;
            border: none;
            color: white;
            font-size: 1rem;
            font-weight: bolder;
            border-radius: 10px;
            display: block;
            text-align: center;
            cursor: pointer;
        }
        button:hover {
            background-color: deepskyblue;
        }

        .main__container {
            width: 100%;
            padding: 1rem;
            max-width: 400px;
            margin: 0 auto;
        }

        .btn-opr__wrapper {
            display: grid;
            grid-template-columns: 1fr 1fr;
            grid-gap: .5rem;
        }
        .btn-opr__wrapper button {
            width: 50px;
            font-size: 2rem;
        }

        table {
            display: block;
            border-collapse: collapse;
            width: 100%;
        }
        table.hide {
            display: none;
        }

        th, td {
            border: 1px solid gray;
            text-align: center;
            padding: .25rem .5rem;
            width: 200px;
        }

        /*layout*/
        .flex {
            display: flex;
        }
        .flex.col {
            flex-direction: column;
        }
        .mb-2 {
            margin-bottom: .5rem;
        }
        .ml-2 {
            margin-left: .5rem;
        }
        .mt-4 {
            margin-top: 1rem;
        }
        .h-100 {
            height: 100%;
        }
        .w-100 {
            width: 100%;
        }
        .flex-grow-1 {
            flex-grow: 1;
        }
    </style>
</head>
<body>

<div class="main__container">
    <div class="flex w-100">
        <div class="flex col flex-grow-1">
            <input class="mb-2" type="number" id="input-a" name="a" placeholder="Input first number" required />
            <input class="mb-2" type="number" id="input-b" name="b" placeholder="Input second number" required />
            <button id="btn-gen">Generate</button>
        </div>
    </div>

    <table class="mt-4 hide">
        <thead>
        <tr>
            <th>Number</th>
            <th>Odd / Even</th>
        </tr>
        </thead>
        <tbody id="table-body">

        </tbody>
    </table>
</div>

<script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
<script>
    const aElm = document.querySelector('#input-a');
    const bElm = document.querySelector('#input-b');
    const btnElm = document.querySelector('#btn-gen');
    const tblElm = document.querySelector('table');
    const tblBodyElm = document.querySelector('#table-body');

    function capitalizeFirstLetter(string) {
        return string.charAt(0).toUpperCase() + string.slice(1);
    }

    btnElm.addEventListener('click', () => {
        if(aElm.value != '' && bElm.value != '') {
            console.log('generating...');

            axios
                .post(
                    '/generate',
                    { a: aElm.value, b: bElm.value }
                )
                .then((res) => {
                    console.log(res.data);
                    const res_arr = res.data;
                    let tableContent = '';

                    res_arr.forEach((item) => {
                        tableContent +=
                            `<tr>
                                <td>${item.number}</td>
                                <td>${capitalizeFirstLetter(item.odd_even)}</td>
                             </tr>`;
                    })

                    tblBodyElm.innerHTML = tableContent;
                    tblElm.classList.remove('hide');
                })
                .catch((err) => {
                    console.error(err);
                })
        }else{
            console.warn('input field first!');
        }
    })

</script>

</body>
</html>