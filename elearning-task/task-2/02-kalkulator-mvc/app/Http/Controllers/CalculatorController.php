<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Exception;

class CalculatorController extends Controller
{
    public function index()
    {
        return view('calculator');
    }

    public function calculate(Request $request)
    {
        $a = floatval($request->input('a'));
        $b = floatval($request->input('b'));
        $opr = $request->input('opr');
        $result = null;

        try
        {
            switch ($opr)
            {
                case "+":
                    $result = $a + $b;
                    break;
                case "-":
                    $result = $a - $b;
                    break;
                case "*":
                    $result = $a * $b;
                    break;
                case "/":
                    if($b == 0) {
                        throw new Exception();
                    }
                    $result = $a / $b;
                    break;
                default:
                    throw new Exception();
            }
        }
        catch (Exception $e)
        {
            $result = 'tidak bisa dilakukan';
        }

        return json_encode(['result' => $result]);
    }
}
