<html>
<head>
    <title>Simple Calculator MVC</title>

    <style>
        body {
            font-size: 16px;
            font-family: sans-serif;
        }
        input {
            border: 1px solid lightgray;
            padding: .5rem 1rem;
            font-size: 1rem;
            border-radius: 10px;
            outline: none;
            display: block;
            height: 50px;
        }
        button {
            background-color: dodgerblue;
            padding: 1rem;
            border: none;
            color: white;
            font-size: 1rem;
            font-weight: bolder;
            border-radius: 10px;
            display: block;
            text-align: center;
            cursor: pointer;
        }
        button:hover {
            background-color: deepskyblue;
        }

        .main__container {
            width: 100%;
            padding: 1rem;
            max-width: 400px;
            position: absolute;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
        }

        .btn-opr__wrapper {
            display: grid;
            grid-template-columns: 1fr 1fr;
            grid-gap: .5rem;
        }
        .btn-opr__wrapper button {
            width: 50px;
            font-size: 2rem;
        }

        /*layout*/
        .flex {
            display: flex;
        }
        .flex.col {
            flex-direction: column;
        }
        .mb-2 {
            margin-bottom: .5rem;
        }
        .ml-2 {
            margin-left: .5rem;
        }
        .h-100 {
            height: 100%;
        }
        .w-100 {
            width: 100%;
        }
        .flex-grow-1 {
            flex-grow: 1;
        }
    </style>
</head>
<body>

<div class="main__container">
    <div class="flex w-100">
        <div class="flex col flex-grow-1">
            <input class="mb-2" type="number" id="input-a" name="a" placeholder="Input first number" required />
            <input class="mb-2" type="number" id="input-b" name="b" placeholder="Input second number" required />
            <input type="hidden" value="" id="input-opr" />
            <input type="text" id="input-result" disabled/>
        </div>
        <div class="btn-opr__wrapper ml-2">
            <button data-opr="+" id="btn-plus">+</button>
            <button data-opr="-" id="btn-min">-</button>
            <button data-opr="*" id="btn-mul">*</button>
            <button data-opr="/" id="btn-div">/</button>
        </div>
    </div>
</div>

<script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
<script>
    const aElm = document.querySelector('#input-a');
    const bElm = document.querySelector('#input-b');
    const oprElm = document.querySelector('#input-opr');
    const resElm = document.querySelector('#input-result');
    const buttons = document.querySelectorAll('button');

    buttons.forEach((elm) => {
       elm.addEventListener('click', () => {
           const oprAtt = elm.getAttribute('data-opr');
           oprElm.value = oprAtt;

           if(aElm.value != '' && bElm.value != '') {
               console.log('calculating...')
               // send ajax request

               axios
                   .post(
                       '/calculate',
                       { a: aElm.value, b: bElm.value, opr: oprElm.value }
                   )
                   .then((res) => {
                        console.log(res.data);
                        let result = res.data.result;
                        resElm.value = result;
                   })
                   .catch((err) => {
                       console.err(err);
                   })

           }else{
               console.warn('input field first!');
           }
       })
    });

</script>

</body>
</html>