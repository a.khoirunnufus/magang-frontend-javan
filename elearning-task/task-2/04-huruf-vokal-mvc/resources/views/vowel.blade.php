<html>
<head>
    <title>Vowel MVC</title>

    <style>
        body {
            font-size: 16px;
            font-family: sans-serif;
        }
        input {
            border: 1px solid lightgray;
            padding: .5rem 1rem;
            font-size: 1rem;
            border-radius: 10px;
            outline: none;
            display: block;
            height: 50px;
        }
        button {
            background-color: dodgerblue;
            padding: 1rem;
            border: none;
            color: white;
            font-size: 1rem;
            font-weight: bolder;
            border-radius: 10px;
            display: block;
            text-align: center;
            cursor: pointer;
        }
        button:hover {
            background-color: deepskyblue;
        }

        .main__container {
            width: 100%;
            padding: 1rem;
            max-width: 400px;
            margin: 0 auto;
        }

        textarea {
            font-family: sans-serif;
            border: 1px solid lightgray;
            padding: .5rem 1rem;
            font-size: 1rem;
            border-radius: 10px;
            outline: none;
            display: block;
            height: 50px;
        }

        #result-text {
            font-size: 1.25rem;
            text-align: left;
        }

        /*layout*/
        .flex {
            display: flex;
        }
        .flex.col {
            flex-direction: column;
        }
        .mb-2 {
            margin-bottom: .5rem;
        }
        .mb-4 {
            margin-bottom: 1rem;
        }
        .ml-2 {
            margin-left: .5rem;
        }
        .mt-4 {
            margin-top: 1rem;
        }
        .h-100 {
            height: 100%;
        }
        .w-100 {
            width: 100%;
        }
        .flex-grow-1 {
            flex-grow: 1;
        }
    </style>
</head>
<body>

<div class="main__container">
    <div class="flex w-100 mb-4">
        <div class="flex col w-100">
            <textarea class="mb-2" id="input-text" placeholder="Type something..."></textarea>
            <button id="btn-gen">Count</button>
        </div>
    </div>
    <div>
        <p id="result-text"></p>
    </div>
</div>

<script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
<script>
    const textElm = document.querySelector('#input-text');
    const btnElm = document.querySelector('#btn-gen');
    const resTextElm = document.querySelector('#result-text');

    btnElm.addEventListener('click', () => {
        if(textElm.value == '') {
            console.warn('Input field first!');
        }else{
            console.log('counting...');

            axios
                .post(
                    '/count',
                    { text: textElm.value }
                )
                .then((res) => {
                    console.log(res.data);

                    resTextElm.innerHTML = res.data.resultText;
                })
                .catch((error) => {
                    if (error.response) {
                        // The request was made and the server responded with a status code
                        // that falls out of the range of 2xx
                        console.log(error.response.data);
                        console.log(error.response.status);
                        console.log(error.response.headers);
                    } else if (error.request) {
                        // The request was made but no response was received
                        // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
                        // http.ClientRequest in node.js
                        console.log(error.request);
                    } else {
                        // Something happened in setting up the request that triggered an Error
                        console.log('Error', error.message);
                    }
                    console.log(error.config);
                })
        }
    })

</script>

</body>
</html>