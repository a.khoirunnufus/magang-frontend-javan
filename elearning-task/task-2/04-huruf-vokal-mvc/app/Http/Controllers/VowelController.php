<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class VowelController extends Controller
{
    public function index()
    {
        return view('vowel');
    }

    public function count(Request $request)
    {
        // VALIDASI XSS
        $text = strip_tags($request->input('text'));

        $kata_arr = str_split(strtolower($text));
        $vokal_arr = array('a', 'i', 'u', 'e', 'o');
        $res_vokal_arr = array();
        $res_jumlah_vokal = 0;

        foreach($kata_arr as $huruf)
        {
            if(in_array($huruf, $vokal_arr) && !in_array($huruf, $res_vokal_arr))
            {
                array_push($res_vokal_arr, $huruf);
                $res_jumlah_vokal ++;
            }
        }

        $res_vokal_text = implode(', ', $res_vokal_arr);
        $result_text = "Input \"".$text."\", terdapat ".$res_jumlah_vokal." huruf vokal yaitu ".$res_vokal_text.".";

        return json_encode([ 'resultText' => $result_text ]);
    }
}
